#!/bin/bash

# Определение цветов по умолчанию
default_column1_background=6
default_column1_font_color=1
default_column2_background=2
default_column2_font_color=4

# Функция для вывода текста с заданными цветами
print_colored_text() {
    local text="$1"
    local background="$2"
    local font_color="$3"
    
    local bg_code=""
    local font_code=""
    
    case "$background" in
        1) bg_code="47";;
        2) bg_code="41";;
        3) bg_code="42";;
        4) bg_code="44";;
        5) bg_code="45";;
        6) bg_code="40";;
    esac

    case "$font_color" in
        1) font_code="97";;
        2) font_code="91";;
        3) font_code="92";;
        4) font_code="94";;
        5) font_code="95";;
        6) font_code="30";;
    esac

    if [ -z "$bg_code" ]; then
        bg_code="40"  # По умолчанию черный фон
    fi

    if [ -z "$font_code" ]; then
        font_code="30"  # По умолчанию черный цвет шрифта
    fi

    echo -e "\e[${bg_code};${font_code}m$text\e[0m"
}

# Функция для вывода названия и значения с заданными цветами
print_colored_pair() {
    local name="$1"
    local value="$2"
    local name_bg="$3"
    local name_font="$4"
    local value_bg="$5"
    local value_font="$6"

    echo -n "$(print_colored_text "$name" "$name_bg" "$name_font") = "
    echo "$(print_colored_text "$value" "$value_bg" "$value_font")"
}

# Проверка наличия конфигурационного файла
if [ -f "config.cfg" ]; then
    source "config.cfg"
else
    # Если файла нет, устанавливаем цветовую схему по умолчанию
    column1_background="$default_column1_background"
    column1_font_color="$default_column1_font_color"
    column2_background="$default_column2_background"
    column2_font_color="$default_column2_font_color"
fi

# Если цвета не определены в config.cfg, используйте цвета по умолчанию
if [ -z "$column1_background" ]; then
    column1_background="$default_column1_background"
fi

if [ -z "$column1_font_color" ]; then
    column1_font_color="$default_column1_font_color"
fi

if [ -z "$column2_background" ]; then
    column2_background="$default_column2_background"
fi

if [ -z "$column2_font_color" ]; then
    column2_font_color="$default_column2_font_color"
fi

# Получаем информацию о системе
HOSTNAME=$(hostname)
TIMEZONE=$(timedatectl show --property=Timezone --value)
TIMEZONE_NAME=$(timedatectl show --property=Timezone --value | cut -d' ' -f1)
USER=$(whoami)
OS=$(lsb_release -d | awk -F"\t" '{print $2}')
DATE=$(date "+%d %B %Y %T")
UPTIME_HOURS=$(uptime -p | awk -F' ' '{print $2}')
UPTIME_MINUTES=$(uptime -p | awk -F' ' '{print $4}')

# Получаем время запуска системы
UPTIME_START=$(date -d "$(uptime -s)" +%s)

# Получаем текущее время
CURRENT_TIME=$(date +%s)

# Вычисляем разницу в секундах
UPTIME_SEC=$((CURRENT_TIME - UPTIME_START))

IP=$(ip -4 addr show | grep inet | awk '{print $2}' | cut -d '/' -f 1 | head -n 1)
MASK=$(ip -4 addr show | grep inet | awk '{print $2}' | awk -F'/' 'NR==1{print $2}')
GATEWAY=$(ip route | grep default | awk '{print $3}')
RAM_TOTAL=$(free -m | awk '/Mem:/ {printf "%.3f GB", $2/1024}')
RAM_USED=$(free -m | awk '/Mem:/ {printf "%.3f GB", $3/1024}')
RAM_FREE=$(free -m | awk '/Mem:/ {printf "%.3f GB", $4/1024}')
SPACE_ROOT=$(df -h / | awk '/\// {printf "%.2f MB", $2*1024}')
SPACE_ROOT_USED=$(df -h / | awk '/\// {printf "%.2f MB", $3*1024}')
SPACE_ROOT_FREE=$(df -h / | awk '/\// {printf "%.2f MB", $4*1024}')

# Вывод информации на экран с заданными цветами
print_colored_pair "HOSTNAME" "$HOSTNAME" "$column1_background" "$column1_font_color" "$column2_background" "$column2_font_color"
print_colored_pair "TIMEZONE" "$TIMEZONE ($TIMEZONE_NAME, UTC$(date +%z | sed 's/^/+/' | sed 's/00/0/'))" "$column1_background" "$column1_font_color" "$column2_background" "$column2_font_color"
print_colored_pair "USER" "$USER" "$column1_background" "$column1_font_color" "$column2_background" "$column2_font_color"
print_colored_pair "OS" "$OS" "$column1_background" "$column1_font_color" "$column2_background" "$column2_font_color"
print_colored_pair "DATE" "$DATE" "$column1_background" "$column1_font_color" "$column2_background" "$column2_font_color"
print_colored_pair "UPTIME" "$UPTIME_HOURS hours $UPTIME_MINUTES minutes" "$column1_background" "$column1_font_color" "$column2_background" "$column2_font_color"
print_colored_pair "UPTIME_SEC" "$UPTIME_SEC seconds" "$column1_background" "$column1_font_color" "$column2_background" "$column2_font_color"
print_colored_pair "IP" "$IP" "$column1_background" "$column1_font_color" "$column2_background" "$column2_font_color"
print_colored_pair "MASK" "$MASK" "$column1_background" "$column1_font_color" "$column2_background" "$column2_font_color"
print_colored_pair "GATEWAY" "$GATEWAY" "$column1_background" "$column1_font_color" "$column2_background" "$column2_font_color"
print_colored_pair "RAM_TOTAL" "$RAM_TOTAL" "$column1_background" "$column1_font_color" "$column2_background" "$column2_font_color"
print_colored_pair "RAM_USED" "$RAM_USED" "$column1_background" "$column1_font_color" "$column2_background" "$column2_font_color"
print_colored_pair "RAM_FREE" "$RAM_FREE" "$column1_background" "$column1_font_color" "$column2_background" "$column2_font_color"
print_colored_pair "SPACE_ROOT" "$SPACE_ROOT" "$column1_background" "$column1_font_color" "$column2_background" "$column2_font_color"
print_colored_pair "SPACE_ROOT_USED" "$SPACE_ROOT_USED" "$column1_background" "$column1_font_color" "$column2_background" "$column2_font_color"
print_colored_pair "SPACE_ROOT_FREE" "$SPACE_ROOT_FREE" "$column1_background" "$column1_font_color" "$column2_background" "$column2_font_color"

# Вывод цветовой схемы
echo

# Функция для получения названия цвета по его коду
get_color_name() {
    local color_code="$1"
    case "$color_code" in
        "1") echo "white";;
        "2") echo "red";;
        "3") echo "green";;
        "4") echo "blue";;
        "5") echo "purple";;
        "6") echo "black";;
        "default") echo "default";;  # Добавляем название для цвета по умолчанию
        *) echo "unknown";;
    esac
}

# Функция для вывода цветовой настройки с учетом цвета по умолчанию
print_color_setting() {
    local setting_name="$1"
    local setting_value="$2"
    local default_value="$3"
    local color_code="$4"
    local color_name=$(get_color_name "$color_code")
    local default_color_name=$(get_color_name "$default_value")

    if [ "$setting_value" = "$default_value" ]; then
        echo "$setting_name = default ($default_color_name)"
    else
        echo "$setting_name = $setting_value ($color_name)"
    fi
}

# Выводим цветовую схему
print_color_setting "Column 1 background" "${column1_background:-$default_column1_background}" "$default_column1_background" "$column1_background"
print_color_setting "Column 1 font color" "${column1_font_color:-$default_column1_font_color}" "$default_column1_font_color" "$column1_font_color"
print_color_setting "Column 2 background" "${column2_background:-$default_column2_background}" "$default_column2_background" "$column2_background"
print_color_setting "Column 2 font color" "${column2_font_color:-$default_column2_font_color}" "$default_column2_font_color" "$column2_font_color"
