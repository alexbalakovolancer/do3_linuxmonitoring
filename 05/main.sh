#!/bin/bash

# Проверка на наличие аргумента
if [ $# -eq 0 ]; then
  echo "Предупреждение: Не указан путь, до анализируемой папки (например /var/log/). Будет использована текущая директория."
  directory="."
else
  # Проверка, что аргумент является директорией
  if [ ! -d "$1" ]; then
    echo "Ошибка: Указанный аргумент не является директорией."
    exit 1
  fi
  directory="$1"
fi

# Запускаем замер времени
start_time=$(date +%s.%N)

# Получение информации о директории
total_folders=$(find "$directory" -type d | wc -l)
total_files=$(find "$directory" -type f | wc -l)
config_files=$(find "$directory" -type f -name "*.conf" | wc -l)
text_files=$(find "$directory" -type f -exec file {} \; | grep -i 'text' | wc -l)
executable_files=$(find "$directory" -type f -executable | wc -l)
log_files=$(find "$directory" -type f -name "*.log" | wc -l)
archive_files=$(find "$directory" -type f \( -name "*.zip" -o -name "*.tar.gz" \) | wc -l)
symbolic_links=$(find "$directory" -type l | wc -l)

# Получение топ-5 каталогов с наибольшим размером
top_folders=$(du -h --max-depth=1 "$directory" 2>/dev/null | sort -rh | head -n 6 | tail -n 5)

# Вывод информации
echo "Total number of folders (including all nested ones) = $total_folders"
echo "TOP 5 folders of maximum size arranged in descending order (path and size):"
count=1
while IFS= read -r line; do
  size=$(echo "$line" | cut -f1)
  path=$(echo "$line" | cut -f2-)
  echo "$count - $path, $size"
  count=$((count+1))
done <<< "$top_folders"

# Проверка, есть ли файлы для вывода
if [ $total_files -eq 0 ]; then
  # Ничего не делаем, не выводим "etc up to 5"
  :
else
  # Получение топ-10 файлов с наибольшим размером
  top_files=$(find "$directory" -type f -exec du -h {} + 2>/dev/null | sort -rh | head -n 10)
  
  echo "Total number of files = $total_files"
  echo "Number of:"
  echo "Configuration files (with the .conf extension) = $config_files"
  echo "Text files = $text_files"
  echo "Executable files = $executable_files"
  echo "Log files (with the extension .log) = $log_files"
  echo "Archive files = $archive_files"
  echo "Symbolic links = $symbolic_links"
  
  echo "TOP 10 files of maximum size arranged in descending order (path, size, and type):"
  count=1
  while IFS= read -r line; do
    path=$(echo "$line" | cut -d$'\t' -f2-)
    if [ -e "$path" ]; then
      size=$(echo "$line" | awk '{print $1}')
      file_extension="${path##*.}" # Получаем расширение файла
      echo "$count - $path, $size, $file_extension"
      count=$((count+1))
    fi
  done <<< "$top_files"
fi

# Получение топ-10 исполняемых файлов с наибольшим размером и их хешей MD5
top_executables=$(find "$directory" -type f -executable -exec du -h {} + 2>/dev/null | sort -rh | head -n 10)

# Вывод заголовка для исполняемых файлов
if [ -n "$top_executables" ]; then
  echo "TOP 10 executable files of the maximum size arranged in descending order (path, size and MD5 hash of file):"
  count=1
  while IFS= read -r line; do
    path=$(echo "$line" | cut -d$'\t' -f2-)
    size=$(echo "$line" | awk '{print $1}')
    md5_hash=$(md5sum "$path" 2>/dev/null | awk '{print $1}')
    echo "$count - $path, $size, $md5_hash"
    count=$((count+1))
  done <<< "$top_executables"
fi

# Время выполнения скрипта
end_time=$(date +%s.%N)
execution_time=$(echo "scale=1; ($end_time - $start_time) / 1" | bc)

# Вывод времени выполнения
echo "Script execution time (in seconds) = $execution_time"
