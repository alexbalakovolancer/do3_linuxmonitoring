#!/bin/bash

# Собираем информацию
HOSTNAME=$(hostname)
TIMEZONE=$(timedatectl show --property=Timezone --value)
TIMEZONE_NAME=$(timedatectl show --property=Timezone --value | cut -d' ' -f1)
USER=$(whoami)
OS=$(lsb_release -d | awk -F"\t" '{print $2}')
DATE=$(date "+%d %B %Y %T")
UPTIME_HOURS=$(uptime -p | awk -F' ' '{print $2}')
UPTIME_MINUTES=$(uptime -p | awk -F' ' '{print $4}')

# Получаем время запуска системы
UPTIME_START=$(date -d "$(uptime -s)" +%s)

# Получаем текущее время
CURRENT_TIME=$(date +%s)

# Вычисляем разницу в секундах
UPTIME_SEC=$((CURRENT_TIME - UPTIME_START))

IP=$(ip -4 addr show | grep inet | awk '{print $2}' | cut -d '/' -f 1 | head -n 1)
MASK=$(ip -4 addr show | grep inet | awk '{print $2}' | cut -d '/' -f 2 | head -n 1)
GATEWAY=$(ip route | grep default | awk '{print $3}')
RAM_TOTAL=$(free -m | awk '/Mem:/ {printf "%.3f GB", $2/1024}')
RAM_USED=$(free -m | awk '/Mem:/ {printf "%.3f GB", $3/1024}')
RAM_FREE=$(free -m | awk '/Mem:/ {printf "%.3f GB", $4/1024}')
SPACE_ROOT=$(df -h / | awk '/\// {printf "%.2f MB", $2*1024}')
SPACE_ROOT_USED=$(df -h / | awk '/\// {printf "%.2f MB", $3*1024}')
SPACE_ROOT_FREE=$(df -h / | awk '/\// {printf "%.2f MB", $4*1024}')

# Выводим информацию на экран
echo "HOSTNAME = $HOSTNAME"
echo "TIMEZONE = $TIMEZONE ($TIMEZONE_NAME, UTC$(date +%z | sed 's/^/+/' | sed 's/00/0/'))"
echo "USER = $USER"
echo "OS = $OS"
echo "DATE = $DATE"
echo "UPTIME = $UPTIME_HOURS hours $UPTIME_MINUTES minutes"
echo "UPTIME_SEC = $UPTIME_SEC seconds"
echo "IP = $IP"
echo "MASK = $MASK"
echo "GATEWAY = $GATEWAY"
echo "RAM_TOTAL = $RAM_TOTAL"
echo "RAM_USED = $RAM_USED"
echo "RAM_FREE = $RAM_FREE"
echo "SPACE_ROOT = $SPACE_ROOT"
echo "SPACE_ROOT_USED = $SPACE_ROOT_USED"
echo "SPACE_ROOT_FREE = $SPACE_ROOT_FREE"

# Предложение записать данные в файл
read -p "Хотите сохранить данные в файл (Y/N)? " choice
if [ "$choice" == "Y" ] || [ "$choice" == "y" ]; then
    TIMESTAMP=$(date "+%d_%m_%y_%H_%M_%S")
    FILENAME="${TIMESTAMP}.status"
    {
        echo "HOSTNAME = $HOSTNAME"
        echo "TIMEZONE = $TIMEZONE ($TIMEZONE_NAME, UTC$(date +%z | sed 's/^/+/' | sed 's/00/0/'))"
        echo "USER = $USER"
        echo "OS = $OS"
        echo "DATE = $DATE"
        echo "UPTIME = $UPTIME_HOURS hours $UPTIME_MINUTES minutes"
        echo "UPTIME_SEC = $UPTIME_SEC seconds"
        echo "IP = $IP"
        echo "MASK = $MASK"
        echo "GATEWAY = $GATEWAY"
        echo "RAM_TOTAL = $RAM_TOTAL"
        echo "RAM_USED = $RAM_USED"
        echo "RAM_FREE = $RAM_FREE"
        echo "SPACE_ROOT = $SPACE_ROOT"
        echo "SPACE_ROOT_USED = $SPACE_ROOT_USED"
        echo "SPACE_ROOT_FREE = $SPACE_ROOT_FREE"
    } > "$FILENAME"
    echo "Данные сохранены в файл $FILENAME"
else
    echo "Данные не сохранены."
fi
