#!/bin/bash

# Функция для проверки, является ли строка числом
script01() {
  re='^[0-9]+$'
  if [[ $1 =~ $re ]]; then
    return 0
  else
    return 1
  fi
}

# Используем функцию для проверки числа
if script01 "$1"; then
  exit 0
else
  exit 1
fi
