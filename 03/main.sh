#!/bin/bash

# Проверка на количество параметров
if [ $# -ne 4 ]; then
    echo "Введите 4 параметра цифрами от 1 до 6: 1.фон названий 2.цвет шрифта 3.фон значений 4.цвет шрифта значений"
    exit 1
fi

# Параметры цветов и фонов
background_names=$1
font_color_names=$2
background_values=$3
font_color_values=$4

# Проверка на совпадение цветов и фонов
if [ "$background_names" == "$font_color_names" ] || [ "$background_values" == "$font_color_values" ]; then
    echo "Ошибка: Цвета и фоны не могут совпадать."
    exit 1
fi

# Функция для вывода текста с заданными цветами и фонами
print_colored_text() {
    local text="$1"
    local background="$2"
    local font_color="$3"
    
    # ANSI escape codes for color and background
    local bg_code=""
    local font_code=""
    case "$background" in
        1) bg_code="47";;
        2) bg_code="41";;
        3) bg_code="42";;
        4) bg_code="44";;
        5) bg_code="45";;
        6) bg_code="40";;
    esac

    case "$font_color" in
        1) font_code="97";;
        2) font_code="91";;
        3) font_code="92";;
        4) font_code="94";;
        5) font_code="95";;
        6) font_code="30";;
    esac

    echo -n -e "\e[${bg_code};${font_code}m${text}\e[0m"
}

# Функция для вывода названий и значений с заданными цветами и фонами на одной строке
print_colored_pair() {
    local name="$1"
    local value="$2"
    local bg_value="$3"
    local font_value="$4"

    print_colored_text "$name = " "$background_names" "$font_color_names"
    print_colored_text "$value" "$bg_value" "$font_color_values"
    echo
}

# Собираем информацию и выводим с заданными цветами и фонами
print_colored_pair "HOSTNAME" "$(hostname)" "$background_values" "$font_color_values"
print_colored_pair "TIMEZONE" "$(timedatectl show --property=Timezone --value)" "$background_values" "$font_color_values"
print_colored_pair "USER" "$(whoami)" "$background_values" "$font_color_values"
print_colored_pair "OS" "$(lsb_release -d | awk -F'\t' '{print $2}')" "$background_values" "$font_color_values"
print_colored_pair "DATE" "$(date "+%d %B %Y %T")" "$background_values" "$font_color_values"
print_colored_pair "UPTIME" "$(uptime -p | awk -F' ' '{print $2}') hours $(uptime -p | awk -F' ' '{print $4}') minutes" "$background_values" "$font_color_values"
print_colored_pair "UPTIME_SEC" "$((CURRENT_TIME - UPTIME_START)) seconds" "$background_values" "$font_color_values"
print_colored_pair "IP" "$(ip -4 addr show | grep inet | awk '{print $2}' | cut -d'/' -f 1 | head -n 1)" "$background_values" "$font_color_values"
print_colored_pair "MASK" "$(ip -4 addr show | grep inet | awk '{print $2}' | cut -d'/' -f 2 | head -n 1)" "$background_values" "$font_color_values"
print_colored_pair "GATEWAY" "$(ip route | grep default | awk '{print $3}')" "$background_values" "$font_color_values"
print_colored_pair "RAM_TOTAL" "$(free -m | awk '/Mem:/ {printf "%.3f GB", $2/1024}')" "$background_values" "$font_color_values"
print_colored_pair "RAM_USED" "$(free -m | awk '/Mem:/ {printf "%.3f GB", $3/1024}')" "$background_values" "$font_color_values"
print_colored_pair "RAM_FREE" "$(free -m | awk '/Mem:/ {printf "%.3f GB", $4/1024}')" "$background_values" "$font_color_values"
print_colored_pair "SPACE_ROOT" "$(df -h / | awk '/\// {printf "%.2f MB", $2*1024}')" "$background_values" "$font_color_values"
print_colored_pair "SPACE_ROOT_USED" "$(df -h / | awk '/\// {printf "%.2f MB", $3*1024}')" "$background_values" "$font_color_values"
print_colored_pair "SPACE_ROOT_FREE" "$(df -h / | awk '/\// {printf "%.2f MB", $4*1024}')" "$background_values" "$font_color_values"
